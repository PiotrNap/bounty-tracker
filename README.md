# Gimbal Bounty Experiment
## Gimbalabs | April 2022

## Why this project
I built this proof of concept to provide transparency on how I am using my Founder's allocation of Gimbal tokens, to share insight into what I am working on, and to share that work with anyone looking for an entrypoint into building on Cardano.

All of the Gimbals that I have received (and have not yet given away) are at this address: addr1qy3xh5h4gjt6z69jqvrt04l0g2d82h4s6cf0xvu7t2nz95j6jnltpt22t0myhsuk9s6lyn74yxmlxakt27ylr9ykh30qz97lf8

I will keep posting bounties until all of my intial Gimbals are used as rewards for completing projects. To be sure, I am going to try to complete my own bounties, and therefore have a record of "earning" some Gimbals. This, I think, is something special about the organization we are building: work will get done if it's worth doing.

## How to use:

### 1. Grab this repo
```
git clone https://gitlab.com/gimbalabs/jamesdunseith/gimbal-tracker
cd gimbal-tracker
npm install
npm run start
```

Your site is now running at http://localhost:8000!

### 2. Use with Cardano `testnet` or `mainnet`
#### in `/src/components/WalletButton/WalletButton.jsx`, look for
```
if ((await window.cardano.getNetworkId()) === 0) return true;
```
- Testnet -> `0` | Mainnet -> `1`


## This project is built with
1. [Gatsby JS](https://www.gatsbyjs.com/docs/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)
2. Chakra UI
3. Easy Peasy for local state management

See `package.json` for full details.