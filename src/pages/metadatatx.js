import React, { useEffect, useState } from "react"
import { graphql } from "gatsby"
import { useStoreState } from "easy-peasy"
import { fromBech32, fromHex, toStr } from "../utils/converter"
import { Flex, Heading, Text, Box } from "@chakra-ui/react"
import { TxForm } from "../components/TxForm"

import { simpleTx } from "../cardano/simple-tx"

import useWallet from "../hooks/useWallet"
import { serializeTxUnspentOutput, valueToAssets } from "../cardano/transaction"

// from connected wallet, get all UTXOs
function getWalletUtxoStrings(wallet) {
  const utxoStrings = wallet.utxos
    .map((utxo) => serializeTxUnspentOutput(utxo).output())
    .map((txOut) => valueToAssets(txOut.amount()))
  return [...new Set(utxoStrings)]
}

// given a hex-encoded Unit in a Value pair, return the policyID
function getPolicyId(unit) {
  let id = ""
  if (unit == "lovelace") {
    return ""
  } else {
    id = unit.slice(0, 56)
  }
  return id
}

// given a hex-encoded Unit in a Value pair, return the asset name
function getTokenName(unit) {
  let name = ""
  if (unit == "lovelace") {
    return "ada"
  } else {
    let temp = fromHex(unit.substring(56))
    name = toStr(temp)
  }
  return name
}

function getQuantity(unit, quantity) {
  if (unit == "lovelace") {
    return quantity / 1000000
  }
  return quantity
}

function getWalletAssetValues(wallet) {
  const utxoStrings = getWalletUtxoStrings(wallet)
  const assetValues = utxoStrings.map((utxoString) =>
    utxoString.map((currentValue) => ({
      policy: getPolicyId(currentValue.unit),
      unit: getTokenName(currentValue.unit),
      quantity: getQuantity(currentValue.unit, currentValue.quantity),
    }))
  )
  return [...new Set(assetValues)]
}

// Look for a particular asset in a wallet
function walletHoldsThisAsset(assetList, policyId, name) {
  let quantity = 0
  console.log("hold me", assetList)
  assetList.map((valueList) => {
    valueList.map((asset) => {
      if (asset.policy === policyId && asset.unit === name) {
        console.log("assets", asset.policy, "name", asset.unit)
        quantity = asset.quantity
      }
    })
  })

  return quantity
}

const MetadataTx = ({ data }) => {
  const edges = data.allMarkdownRemark.edges

  const connected = useStoreState((state) => state.connection.connected)
  const [walletAddress, setWalletAddress] = useState(null)
  const [walletBalance, setWalletBalance] = useState("")
  const [walletAssets, setWalletAssets] = useState([])
  const [walletUtxos, setWalletUtxos] = useState([])
  const { wallet } = useWallet(null)
  const [loading, setLoading] = useState(true)

  const [gimbalTokens, setGimbalTokens] = useState(0)

  // on loading change, if wallet is connected, set wallet address
  useEffect(async () => {
    if (connected && wallet) {
      setWalletBalance(wallet.balance)
      setWalletAddress(wallet.address)
      setWalletAssets(getWalletAssetValues(wallet))
      console.log("what about here", getWalletAssetValues(wallet))
    }
  }, [loading])

  useEffect(async () => {
    if (connected && wallet) {
      const myUtxos = await wallet.utxos
      setWalletUtxos(wallet.utxos)
      setLoading(false)
    }
  }, [wallet])

  useEffect(() => {
    if (connected && wallet) {
      console.log(
        "hello gimbals",
        walletHoldsThisAsset(
          walletAssets,
          "2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30",
          "gimbal"
        )
      )
      setGimbalTokens(
        walletHoldsThisAsset(
          walletAssets,
          "2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30",
          "gimbal"
        )
      )
    }
  }, [walletAssets])

  useEffect(() => {
    if (walletUtxos.length == 0) {
      setLoading(true)
    }
    if (walletAddress == null || walletAddress == "") {
      setLoading(true)
    }
    if (loading) {
      setLoading(false)
    }
  }, [walletUtxos, setWalletAddress])

  const sendMyTransaction = async (formValue) => {
    const { recAddr, metadataMsg, gimbalsToSend } = formValue
    const fromMyWallet = {
      address: fromBech32(walletAddress),
      utxosParam: walletUtxos,
      recAddr: recAddr,
      memo: metadataMsg,
      gimbals: gimbalsToSend,
    }
    const txHash = await simpleTx(fromMyWallet)

    setLoading(true)
  }

  return (
    <>
      <title>demo v0</title>
      <Flex
        w="100%"
        mx="auto"
        direction="column"
        wrap="wrap"
        bg="gl-yellow"
        p="10"
      >
        <Box w="50%" mx="auto" my="5">
          <Heading size="4xl" color="gl-blue" fontWeight="medium" py="5">
            Send Gimbals
          </Heading>
          <Text p="1" color="gl-red" fontSize="sm">
            Wallet Balance: {walletBalance} Ada
          </Text>
          <Text p="1" color="gl-red" fontSize="sm">
            Gimbal Tokens: {gimbalTokens / 1000000}
          </Text>
          <Text p="1" color="gl-red" fontSize="sm">
            Connected Address: {walletAddress}
          </Text>
          <Text p="2" color="gl-blue">
            This tool is still a proof-of-concept and not yet ready for public
            use. Next steps are documented across the site, and (speaking of
            next steps) will soon be listed as bounties.
          </Text>
        </Box>
        <TxForm onSubmitCb={sendMyTransaction} edges={edges} />
      </Flex>
    </>
  )
}

export const query = graphql`
  query GetTitles {
    allMarkdownRemark(sort: { order: ASC, fields: [frontmatter___slug] }) {
      edges {
        node {
          frontmatter {
            title
            slug
          }
        }
      }
    }
  }
`

export default MetadataTx
